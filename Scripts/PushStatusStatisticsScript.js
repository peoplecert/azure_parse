var startDate = new Date("2019-10-14")
var endDate = new Date("2019-10-21")

var allPushNotifications = db.getCollection('_PushStatus').find({_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()

  
print("All Push Notifications from " + startDate + " until " + endDate.toDateString("%Y-%m-%d") + " are " + allPushNotifications + "\n" )
