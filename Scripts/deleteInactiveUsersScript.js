var startDate = new Date("2017-01-01")
var endDate = new Date("2019-07-01")

var allUsers = db.getCollection('_Installation').find({_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var oneChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 1} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var twoChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 2} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var inactiveUsers = db.getCollection('_Installation').find({channels : {"$exists" : false} ,_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var emptyChannels = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 0} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
  
print("All users from " + startDate.toDateString("%Y-%m-%d") + " until " + endDate.toDateString("%Y-%m-%d") + " are " + allUsers + "\n" + "Out of them : \n" + oneChannelUsers  + " are active with 1 channel," + "\n" + twoChannelUsers + " are active with 2 channels \n" + inactiveUsers +" are inactive \n and "+ emptyChannels  + " are signed out") 

var deleteInactiveUsers = true
var deleteLNonActiveUsers = true

    
if (deleteInactiveUsers) {
    if (inactiveUsers > 0) {
        db.getCollection('_Installation').find({channels : {"$exists" : false } , _created_at:{"$gte" : startDate , "$lt" : endDate}}).forEach(function(doc){
            db.getCollection('_Installation').remove({_id: doc._id});
        })
    }
 }
     
 if (deleteLNonActiveUsers) { 
    if (emptyChannels > 0) {
        db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 0} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).forEach(function(doc){
                db.getCollection('_Installation').remove({_id: doc._id});
        })
    }
}

 allUsers = db.getCollection('_Installation').find({_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
 oneChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 1} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
 twoChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 2} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
 inactiveUsers = db.getCollection('_Installation').find({channels : {"$exists" : false} ,_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
 emptyChannels = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 0} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
  
print("All users from " + startDate.toDateString("%Y-%m-%d") + " until " + endDate.toDateString("%Y-%m-%d") + " are " + allUsers + "\n" + "Out of them : \n" + oneChannelUsers  + " are active with 1 channel," + "\n" + twoChannelUsers + " are active with 2 channels \n" + inactiveUsers +" are inactive \n and "+ emptyChannels  + " are signed out") 
