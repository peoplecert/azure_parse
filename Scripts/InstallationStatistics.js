var startDate = new Date("2019-10-14")
var endDate = new Date("2019-10-21")

var allUsers = db.getCollection('_Installation').find({_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var oneChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 1} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var twoChannelUsers = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 2} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var inactiveUsers = db.getCollection('_Installation').find({channels : {"$exists" : false} ,_created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
var emptyChannels = db.getCollection('_Installation').find({channels : {"$exists" : true , "$size" : 0} , _created_at:{"$gte" : startDate , "$lt" : endDate}}).count()
  
print("All users from " + startDate.toDateString("%Y-%m-%d") + " until " + endDate.toDateString("%Y-%m-%d") + " are " + allUsers + "\n" + "Out of them : \n" + oneChannelUsers  + " are active with 1 channel," + "\n" + twoChannelUsers + " are active with 2 channels \n" + inactiveUsers +" are inactive \n and "+ emptyChannels  + " are signed out") 
