using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;
using System.Net.Mime;
using System.Net.Mail;
using System.Net;
using System.Collections;
using System.Threading;

namespace CosmosDbExport
{
    public static class ExportFunction
    {
        /// <summary>
        /// The number of days from which older records will be exported.
        /// </summary>


        private const string ThrottlingErrorMessage = "request rate is large(throttling error)";
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static readonly string CosmosDbConnectionString;
        private static readonly string CosmosDbName;
        private static readonly string CosmosDbPushStatusCollectionName;
        private static readonly string CosmosDbInstallationCollectionName;

        private static readonly string StorageConnectionString;
        private static readonly string PushStatusExportContainerName;
        private static readonly string InstallationExportContainerName;
        private static readonly CloudStorageAccount StorageAccount;
        private static readonly CloudBlobClient BlobClient;


        private static readonly Boolean isLive;
        private static readonly Boolean deletePushStatusDocuments;
        private static readonly Boolean deleteInstallationDocuments;
        private static readonly Boolean exportPushStatusDocuments;
        private static readonly Boolean exportInstallationDocuments;
        private static readonly int NumberOfDaysDeletionThreshold_PushStatus;
        private static readonly int NumberOfDaysDeletionThreshold_Installation;
        private static readonly int NumberOfDeletionDaysArchive;
        private static readonly Boolean logToConsoleActive;
        private static readonly Boolean logToFileActive;
        private static readonly Boolean logDetailToFileActive;

        private static readonly string loggingDirectory = "Logs/";

        private static StreamWriter logsStreamWriter;
        private static string logsFilename;
        private static StreamWriter detailedLogsStreamWriter;
        private static string detailedLogsFilename;

        /// <summary>
        /// Initialize configuration for this function.
        /// </summary>
        static ExportFunction()
        {
            RegisterForUnhandledExceptions();

            isLive = Boolean.Parse(Environment.GetEnvironmentVariable("IsLive"));

            CosmosDbConnectionString = Environment.GetEnvironmentVariable($"CosmosDbConnectionString{(isLive ? "_live" : "")}");
            CosmosDbName = Environment.GetEnvironmentVariable($"CosmosDbName{(isLive ? "_live" : "")}");
            CosmosDbPushStatusCollectionName = Environment.GetEnvironmentVariable($"CosmosDbCollectionName_PushStatus{(isLive ? "_live" : "")}");
            CosmosDbInstallationCollectionName = Environment.GetEnvironmentVariable($"CosmosDbCollectionName_Installation{(isLive ? "_live" : "")}");
            StorageConnectionString = Environment.GetEnvironmentVariable($"StorageConnectionString{(isLive ? "_live" : "")}");
            PushStatusExportContainerName = Environment.GetEnvironmentVariable($"ExportContainerName_PushStatus{(isLive ? "_live" : "")}");
            InstallationExportContainerName = Environment.GetEnvironmentVariable($"ExportContainerName_Installation{(isLive ? "_live" : "")}");
            NumberOfDaysDeletionThreshold_PushStatus = Int32.Parse(Environment.GetEnvironmentVariable($"NumberOfDaysDeletionThreshold_PushStatus{(isLive ? "_live" : "")}"));
            NumberOfDaysDeletionThreshold_Installation = Int32.Parse(Environment.GetEnvironmentVariable($"NumberOfDaysDeletionThreshold_Installation{(isLive ? "_live" : "")}"));


            logToConsoleActive = Boolean.Parse(Environment.GetEnvironmentVariable("LogToConsoleActive"));
            logToFileActive = Boolean.Parse(Environment.GetEnvironmentVariable("LogToFileActive"));
            logDetailToFileActive = Boolean.Parse(Environment.GetEnvironmentVariable("LogDetailToFileActive"));


            deletePushStatusDocuments = Boolean.Parse(Environment.GetEnvironmentVariable("DeletePushStatusDocuments"));
            deleteInstallationDocuments = Boolean.Parse(Environment.GetEnvironmentVariable("DeleteInstallationDocuments"));
            exportPushStatusDocuments = Boolean.Parse(Environment.GetEnvironmentVariable("ExportInstallationDocuments"));
            exportInstallationDocuments = Boolean.Parse(Environment.GetEnvironmentVariable("ExportPushStatusDocuments"));
            NumberOfDeletionDaysArchive = Int32.Parse(Environment.GetEnvironmentVariable("NumberOfDeletionDaysArchive"));
            StorageAccount = CloudStorageAccount.Parse(StorageConnectionString);
            BlobClient = StorageAccount.CreateCloudBlobClient();
        }

        /// <summary>
        /// Actual export method, runs on a schedule
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="log"></param>
        [FunctionName(nameof(ExportFunction))]
        public static async Task Run([TimerTrigger("0 30 11 * Jan Mon", RunOnStartup = true)]TimerInfo myTimer, ILogger log)
        {
            try
            {
                if (!Directory.Exists("Logs"))
                {
                    Directory.CreateDirectory("Logs");
                }
                initLogging();
                startLogging();

                Log($"C# Timer trigger function started ", log, false);

                var mongoClientSettings = MongoClientSettings.FromUrl(new MongoUrl(CosmosDbConnectionString));
                var mongoClient = new MongoClient(mongoClientSettings);
                var mongoDatabase = mongoClient.GetDatabase(CosmosDbName);

                if (deletePushStatusDocuments)
                {
                    await ClearPushStatus(mongoDatabase, log);
                }
                Log("", log, false);
                if (deleteInstallationDocuments)
                {
                    await ClearInstallations(mongoDatabase, log);
                }

                endLogging();
                //SendLogsViaEmail();
            }
            catch (Exception ex)
            {
                Log($"Exception in async Task Run raised {ex.ToString()}", log, false);
                log.LogError(ex, "Exception in async Task Run raised {0}", ex.ToString());
            }
        }


        private static async Task ClearPushStatus(IMongoDatabase mongoDatabase, ILogger log)
        {
            try
            {
                var mongoCollection = mongoDatabase.GetCollection<BsonDocument>(CosmosDbPushStatusCollectionName);
                Log($"Number of days threshold for PushStatus: {NumberOfDaysDeletionThreshold_PushStatus}", log, false);
                //LOG day selected
                Log($"Date selected for PushStatus to check before: {DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_PushStatus))}", log, false);

                var filter = GetPushStatusDeletionFilter(log);
                //LOG Filter used
                Log($"Filter used: {filter.ToString()}", log, false);
                var sort = Builders<BsonDocument>.Sort.Ascending("_created_at");
                var options = new FindOptions<BsonDocument>
                {
                    BatchSize = 20,
                    NoCursorTimeout = true
                };

                //LOG Options for query
                Log($"Options for query: {options.ToJson()}", log, false);
                Log("", log, false);
                await ExportAndDeleteColletionDocumentsAsync(mongoCollection, filter, options, PushStatusExportContainerName, exportPushStatusDocuments, log);
            }
            catch (Exception ex)
            {
                Log($"Exception in ClearPushStatus raised {ex.ToString()}", log, false);
                log.LogError(ex, "Exception in ClearPushStatus raised {0}", ex.ToString());
            }
        }

        private static async Task ClearInstallations(IMongoDatabase mongoDatabase, ILogger log)
        {
            try
            {
                Log($"Number of days threshold for Installation: {NumberOfDaysDeletionThreshold_PushStatus}", log, false);
                //LOG day selected
                Log($"Date selected for Installation to check before: {DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_PushStatus))}", log, false);

                var mongoCollection = mongoDatabase.GetCollection<BsonDocument>(CosmosDbInstallationCollectionName);
                var deletionFilter = GetInstallationDeletionFilter(new DateTime(), new DateTime());
                Log($"Filter used: {deletionFilter.ToString()}", log, false);
                var deletionOptions = new FindOptions<BsonDocument>
                {
                    BatchSize = 20,
                    NoCursorTimeout = true
                };

                Log($"Options for query: {deletionOptions.ToJson()}", log, false);
                Log("", log, false);
                await ExportAndDeleteColletionDocumentsAsync(mongoCollection, deletionFilter, deletionOptions, InstallationExportContainerName, exportInstallationDocuments, log);
            }
            catch (Exception ex)
            {
                Log($"Exception in ClearInstallations raised {ex.ToString()}", log, false);
                log.LogError(ex, "Exception in ClearInstallations raised {0}", ex.ToString());
            }
        }

        /// <summary>
        /// Setup the filter criteria for the documents to be deleted.
        /// </summary>
        /// <returns></returns>
        private static FilterDefinition<BsonDocument> GetPushStatusDeletionFilter(ILogger log)
        {
            try
            {
                var dateThreshold = DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_PushStatus));

                var builder = Builders<BsonDocument>.Filter;

                //IS OK -  USED ONLY FOR TEST
                //var filter = builder.Empty;

                //NOT OK BCS WITH MONGO DRIVER WE ACCESS _PushStatus collection without "collectionName" field
                //SEND MY MS
                var filter = builder.Lt("_created_at", new BsonDateTime(dateThreshold));

                //IS OK -  USED ONLY FOR TEST
                //var filter = builder.Eq("source", "rest");

                //IS   ? -  USED ONLY FOR TEST
                //var filter = builder.Lt("_created_at", new BsonDateTime(dateThreshold));

                //IS OK -  USED ONLY FOR TEST BCS IT FIXED DATETIME
                //DateTime startDate = new DateTime(2019, 10, 9);
                //var filter = builder.Gt("_created_at", new BsonDateTime(new DateTime(2019, 3, 12)));

                //IS OK - TO BE USED
                //var filter = builder.Lt("_created_at", new BsonDateTime(dateThreshold));

                //var filter = builder.Eq("_id", "Fz8dDA7Vnx");

                return filter;
            }catch (Exception ex)
            {
                Log($"Exception in GetPushStatusDeletionFilter raised {ex.ToString()}", log, false);
                log.LogError(ex, "Exception in GetPushStatusDeletionFilter raised {0}", ex.ToString());
            }
            return null;
        }

        private static FilterDefinition<BsonDocument> GetInstallationDeletionFilter(DateTime startDate, DateTime endDate)
        {
            var dateThreshold = DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_Installation));
            var builder = Builders<BsonDocument>.Filter;

            var filter = (builder.Exists("channels", false) | (builder.Exists("channels", true) & builder.Size("channels", 0)))
                & builder.Lt("_created_at", new BsonDateTime(dateThreshold));

            return filter;
        }


        /// <summary>
        /// Returns whether the exception is caused by a throttling response by CosmosDb.
        /// </summary>
        /// <param name="ex"></param>
        private static bool IsThrottled(Exception ex)
        {
            return ex.Message.ToLower().Contains(ThrottlingErrorMessage);
        }


        private static async Task ExportAndDeleteColletionDocumentsAsync(IMongoCollection<BsonDocument> mongoCollection, FilterDefinition<BsonDocument> filter, FindOptions<BsonDocument> options, string exportContainerName, Boolean exportDocuments, ILogger log)
        {
            try
            {
                Log($"Process delete{(exportDocuments ? " and export" : "")} {exportContainerName} Documents started at {DateTime.Now}", log, false);

                var blobContainer = BlobClient.GetContainerReference(exportContainerName);
                var cursor = await mongoCollection.FindAsync(filter, options);
                var exportStartDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_PushStatus + NumberOfDeletionDaysArchive));
                var exportEndDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(NumberOfDaysDeletionThreshold_PushStatus));
                var testDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(870));
                //long recordsCount = await mongoCollection.CountAsync(filter);
                //LOG count of records selected
                //LogToFile($"Number of records Found: {cursor.ToList().Count}");0
                //LogToFile($"Number of records Found: {recordsCount}");

                int deletedDocumentsCount = 0;
                int exportedDocumentsCount = 0;


                while (await cursor.MoveNextAsync())
                    {
                        IEnumerable<BsonDocument> batch = cursor.Current;
                        foreach (var record in batch)
                        {
                            var recordJson = record.ToJson();
                            var recordId = record.GetValue("_id");
                            var recordCreatedAt = record.GetValue("_created_at");
                        object recordUpdatedAt = null;

                        if (record.Contains("_updated_at"))
                        {
                           recordUpdatedAt = record.GetValue("_updated_at");
                        }
                        //LOG moving record basic fields 
                        Log($"Fetched Document with id: {recordId} created at: {recordCreatedAt} and updated at: {recordUpdatedAt}", log, true);
                            if (exportDocuments)
                            {
                                if (recordCreatedAt > exportStartDate && recordCreatedAt < exportEndDate)
                                {
                                    var blob = blobContainer.GetBlockBlobReference($"{mongoCollection.CollectionNamespace}/{recordId}.json");
                                    Task uploadTask = blob.UploadTextAsync(recordJson);
                                    await uploadTask;
                                    //LOG Upload block result
                                    if (!uploadTask.IsCompletedSuccessfully)
                                    {
                                        Log($"Upload failed for Document {recordId} with reason {uploadTask.Exception}", log, true);
                                    }
                                    else
                                    {
                                        exportedDocumentsCount++;
                                    }
                                    Log($"Upload Document {recordId} to {blob.Name}  {(uploadTask.IsCompletedSuccessfully ? "completed" : "failed")}", log, true);
                                }
                            }

                            //LOG Delete Document
                            Log($"Delete Document with id: {recordId} started", log, true);
                            var deleteDocumentElements = new[] {
                            new BsonElement("_id", recordId)
                            }.ToList();
                            var deleteResult = await mongoCollection.DeleteOneAsync(new BsonDocument(deleteDocumentElements));
                            //LOG result
                            Log($"Delete Document {deletedDocumentsCount} with id: {recordId} completed with result {deleteResult}", log, true);
                            if (!deleteResult.IsAcknowledged)
                            {
                                Log($"Delete failed {deleteResult} for Document {recordId}", log, true);
                            }
                            else
                            {
                                deletedDocumentsCount++;
                            }
                        }

                    }
                
                Log($"Process delete{(exportDocuments ? " and export" : "")} {exportContainerName} Documents  finished at {DateTime.Now} with {deletedDocumentsCount} deleted documents {(exportDocuments ? $"and {exportedDocumentsCount} exported documents" : "")} successfully", log, false);
            }
            catch (Exception ex)
            {
      
                if (!IsThrottled(ex))
                {
                    Log($"Exception in ExportAndDeleteColletionDocumentsAsync raised {ex.ToString()}", log, false);
                    log.LogError(ex, "Exception in ExportAndDeleteColletionDocumentsAsync raised {0}", ex.ToString());
                    await ExportAndDeleteColletionDocumentsAsync(mongoCollection,filter,options,exportContainerName,exportDocuments,log);
                    throw;
                }
                else
                {
                    // Thread will wait in between 1.5 secs and 3 secs.
                    Log($"Exception in ExportAndDeleteColletionDocumentsAsync for throttling raised {ex.ToString()}", log, false);
                    log.LogError(ex, "Exception in ExportAndDeleteColletionDocumentsAsync raised {0}", ex.ToString());
                    await Task.Delay(new Random().Next(1500, 3000));
                }
            }
        }

        private static void RegisterForUnhandledExceptions()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);
        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            
            Log("MyHandler caught : " + e.Message,null,false);
            Console.WriteLine("Runtime terminating: {0}", args.IsTerminating);
        }

        //LOGGING 
        public static void initLogging()
        {
            detailedLogsFilename = loggingDirectory + DateTime.Now.ToString("yyMMddhhmm") + "detailed.txt";
            logsFilename = loggingDirectory + DateTime.Now.ToString("yyMMddhhmm") + ".txt";
        }

        public static void startLogging()
        {
        
            detailedLogsStreamWriter = File.AppendText(detailedLogsFilename);
            logsStreamWriter = File.AppendText(logsFilename);
 
        }

        public static void Log(string logMessage, ILogger logger, Boolean isDetailed)
        {
            Log(logMessage, logger, isDetailed, true,true);
        }

        public static void Log(string logMessage, ILogger logger, Boolean isDetailedLogInfo, Boolean logToFile, Boolean logToConsole)
        {

            logMessage = $"{DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ")}  {logMessage}";

            //console logging
            if (logToConsoleActive)
            {
                if (logger != null && logToConsole)
                {
                    logger.LogInformation(logMessage);
                }
            }

            //file logging
            if (logToFile)
            {
                //file not detail logging
                if (logToFileActive && !isDetailedLogInfo)
                {
                    LogTo(logsStreamWriter, logMessage);
                }

                //file detail logging
                if (logDetailToFileActive)
                {
                    LogTo(detailedLogsStreamWriter, logMessage);
                }
            }
        }

        public static void LogTo(StreamWriter logStreamWriter, string logMessage)
        {
            logStreamWriter.WriteLine($"{logMessage}");
            logStreamWriter.Write("\r\n");
            logStreamWriter.Flush();
        }

        public static void endLogging()
        {
            detailedLogsStreamWriter.Close();
            logsStreamWriter.Close();
        }

        public static void SendLogsViaEmail()
        {
            // Specify the file to be attached and sent.
            // This example assumes that a file named Data.xls exists in the
            // current working directory.
            string file = logsFilename;
            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(
                "appleid@peoplecert.org",
               "vasilis.nourmas@peoplecert.org",
               "CosmosDB collections deletion outcome",
               "See the attached log file.");

            message.To.Add(new MailAddress("stelios.kavouras@peoplecert.org"));
            // Create  the file attachment for this email message.
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
            // Add the file attachment to this email message.
            message.Attachments.Add(data);

            //Send the message.
            SmtpClient client = new SmtpClient();
            client.Host = "dc-01.interfranchise.intranet";
            // Add credentials if the SMTP server requires them.
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "appleid@peoplecert.org",
                Password = "31koW-bo"
            };
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateMessageWithAttachment(): {0}",
                      ex.ToString());
            }
            // Display the values in the ContentDisposition for the attachment.
            ContentDisposition cd = data.ContentDisposition;
            Console.WriteLine("Content disposition");
            Console.WriteLine(cd.ToString());
            Console.WriteLine("File {0}", cd.FileName);
            Console.WriteLine("Size {0}", cd.Size);
            Console.WriteLine("Creation {0}", cd.CreationDate);
            Console.WriteLine("Modification {0}", cd.ModificationDate);
            Console.WriteLine("Read {0}", cd.ReadDate);
            Console.WriteLine("Inline {0}", cd.Inline);
            Console.WriteLine("Parameters: {0}", cd.Parameters.Count);
            foreach (DictionaryEntry d in cd.Parameters)
            {
                Console.WriteLine("{0} = {1}", d.Key, d.Value);
            }
            data.Dispose();
        }

    }
}
